const assert = require('assert');
const app = require('../../src/server/app');

describe('\'orderItem\' service', () => {
  it('registered the service', () => {
    const service = app.service('order-item');

    assert.ok(service, 'Registered the service');
  });
});
