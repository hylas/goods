const { authenticate } = require('@feathersjs/authentication').hooks;

const {
  hashPassword, protect
} = require('@feathersjs/authentication-local').hooks;

const processModifyUser = require('../../hooks/process-modify-user');

module.exports = {
  before: {
    all: [],
    find: [ authenticate('jwt') ],
    get: [ authenticate('jwt') ],
    create: [ hashPassword(), (context) => { context.data.isAdmin = context.params.user == null ? context.data.isAdmin : false; return context; } ],
    update: [hashPassword(), authenticate('jwt'), processModifyUser()],
    patch: [hashPassword(), authenticate('jwt'), processModifyUser()],
    remove: [authenticate('jwt'), processModifyUser()]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
