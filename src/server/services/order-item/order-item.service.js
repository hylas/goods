// Initializes the `orderItem` service on path `/order-item`
const createService = require('feathers-mongoose');
const createModel = require('../../models/order-item.model');
const hooks = require('./order-item.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/order-item', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('order-item');

  service.hooks(hooks);
};
