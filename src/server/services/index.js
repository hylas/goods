const users = require('./users/users.service.js');
const order = require('./order/order.service.js');
const orderItem = require('./order-item/order-item.service.js');
const listing = require('./listing/listing.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(order);
  app.configure(orderItem);
  app.configure(listing);
};
