// orderItem-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const orderItem = new Schema({
    listing: { type: Schema.Types.ObjectId, ref: 'listing' },
    quantity: { type: Number }
  }, {
    timestamps: true
  });

  return mongooseClient.model('orderItem', orderItem);
};
