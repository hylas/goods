// users-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const users = new mongooseClient.Schema({

    /**
     * User Properties
     */

    email: { type: String, unique: true, lowercase: true, required: true },
    password: { type: String, required: true },

    fname: { type: String, required: true },
    lname: { type: String, required: true },

    googleId: { type: String },

    facebookId: { type: String },

    /**
     * Customer Properties
     */
    cart: [{
      listing: { type: Schema.Types.ObjectId, ref: 'listing' },
      quantity: { type: Number }
    }],


    /**
     * Admin Properties
     */

    isAdmin: { type: Boolean, default: false },


    /**
     * Merchant Properties
     */

    isStore: { type: Boolean, default: false },

    store: {
      name: { type: String },
      imageUrl: { type: String },
      description: { type: String },
      email: { type: String },
      phone: { type: String },
      address: {
        street: { type: String },
        city: { type: String },
        state: { type: String },
        zip: { type: Number }
      }
    }

  }, {
    timestamps: true
  });

  return mongooseClient.model('users', users);
};
