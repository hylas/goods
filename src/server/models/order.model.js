// order-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;

  const statuses = {
    DELIVERED: 'Delivered',
    SHIPPED: 'In Transit',
    PLACED: 'Awaiting Shipment',
  }

  const order = new Schema({
    shippingAddress: {
      street: { type: String },
      city: { type: String },
      state: { type: String },
      zip: { type: Number }
    },
    cart: [{
      listing: { type: Schema.Types.ObjectId, ref: 'listing' },
      quantity: { type: Number }
    }],
    status: { type: String, default: statuses.PLACED },
    user: { type: Schema.Types.ObjectId, ref: 'users' }
  }, {
    timestamps: true
  });

  return mongooseClient.model('order', order);
};
