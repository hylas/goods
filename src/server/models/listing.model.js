// listing-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const listing = new Schema({
    name: { type: String, required: true },
    price: { type: Number }, // In pennies
    description: { type: String },
    photoUrl: { type: String },
    quantityInStock: { type: Number },
    merchant: { type: Schema.Types.ObjectId, ref: 'users' }
  }, {
    timestamps: true
  });

  return mongooseClient.model('listing', listing);
};
