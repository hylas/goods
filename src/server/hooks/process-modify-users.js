// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return function processModifyUsers (hook) {
      const { data, params } = context;

      user = data;

      // Users can only edit themselves (unless admin)
      if (!params.user.isAdmin && params.user.id != user.id) {
        throw new Error('Permission denied to edit another user')
      }

      // Only admins can set isAdmin to true
      if (!params.user.isAdmin && user.isAdmin == true) {
        throw new Error('Permission denied to change isAdmin property')
      }
    return Promise.resolve(hook);
  };
};
