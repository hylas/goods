const errors = require('@feathersjs/errors');

// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    const { data, params } = context;

    user = data;

    // Users can only edit themselves (unless admin)
    if (params.user != null && !params.user.isAdmin && params.user._id != context.id) {
      throw new errors.BadRequest('Permission denied to edit another user')
    }

    // Only admins can set isAdmin to true
    if (params.user != null && !params.user.isAdmin && user.isAdmin == true) {
      throw new errors.BadRequest('Permission denied to change isAdmin property')
    }

    return context;
  };
};
