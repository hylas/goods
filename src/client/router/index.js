import Vue from 'vue';
import Router from 'vue-router';
import store from '../store';
import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import Register from '../views/Register.vue';
import RegisterStore from '../views/RegisterStore.vue';
import Merchants from '../views/Merchants.vue';
import Merchant from '../views/Merchant.vue';
import Store from '../views/Store.vue';
import Cart from '../views/Cart.vue';
import StoreOrders from '../views/StoreOrders.vue';
import Orders from '../views/Orders.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: { requiresAuth: true }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
    },
    {
      path: '/register-store',
      name: 'registerStore',
      component: RegisterStore,
      meta: { requiresAuth: true }
    },
    {
      path: '/orders',
      name: 'orders',
      component: Orders,
    },
    {
      path: '/store/orders',
      name: 'storeOrders',
      component: StoreOrders,
    },
    {
      path: '/merchant/:id',
      name: 'merchantIndex',
      component: Merchant,
    },
    {
      path: '/merchant',
      name: 'merchant',
      component: Merchants,
    },
    {
      path: '/store/1',
      name: 'store',
      component: Store,
    },
    {
      path: '/cart',
      name: 'cart',
      component: Cart,
    },
    {
      path: '/admin/users',
      component: () => import('../views/admin/Users.vue'),
      meta: { requiresAdmin: true }
    },
    {
      path: '*',
      redirect: '/'
    }
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters['users/current']) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    }
    else {
      next()
    }
  }
  else if (to.matched.some(record => record.meta.requiresAdmin)) {
    if (!store.getters['users/current'] || !store.getters['users/current'].isAdmin) {
      next({
        path: '/'
      })
    }
    else {
      next()
    }
  }
  else {
    next()
  }
});

export default router;
