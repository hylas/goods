const app = require('./src/server/app');
const mongoose = require('mongoose');

const seed = (name, data) => {
  const service = app.service(name);

  created = [];

  service.remove(null).then(() => {
    data.forEach((e) => {
      service.create(e);
      created.push(e);
    });
  });

  return created;
}

  /**
   * User seeds
   */

const users = [
    {
      _id: mongoose.Types.ObjectId("41224d776a326fb40f000001"),
      email: "chris@ufl.edu",
      password: "secret",

      fname: "Chris",
      lname: "Anderson",

      isAdmin: true,
      isStore: false,

      cart: [],
    },
    {
      _id: mongoose.Types.ObjectId("41224d776a326fb40f000002"),
      email: "liam@ufl.edu",
      password: "secret",

      fname: "Liam",
      lname: "Schimdt",

      isAdmin: true,
      isStore: true,

      cart: [],

      store: {
        name: "Liam's Drone Materials",
        description: "Military grade drones and Taylor Swift posters.",
        email: "support@liamsdrones.com",
        phone: "+18507975965",
        address: {
          street: "123 Main St",
          city: "Gainesville",
          state: "FL",
          zip: 32607
        }
      }
    },
    {
      _id: mongoose.Types.ObjectId("41224d776a326fb40f000003"),
      email: "charlotte@ufl.edu",
      password: "secret",

      fname: "Charlotte",
      lname: "Kirk",

      isAdmin: false,
      isStore: true,

      cart: [],

      store: {
        name: "Charlotte's Cookies",
        description: "We buy them directly from the CSE alleyway vending machine!",
        email: "support@charlottescookies.com",
        phone: "+1231231234",
        address: {
          street: "124 Main St",
          city: "Gainesville",
          state: "FL",
          zip: 32607
        }
      }
    },
    {
      _id: mongoose.Types.ObjectId("41224d776a326fb40f000004"),
      email: "randy@ufl.edu",
      password: "secret",

      fname: "Randy",
      lname: "Oram",

      isAdmin: true,
      isStore: true,

      cart: [],

      store: {
        name: "Entranced",
        description: "We sell OG vinyls of EDM's best genre.",
        email: "support@entranced.com",
        phone: "+1",
        address: {
          street: "777 Trance St",
          city: "Gainesville",
          state: "FL",
          zip: 32607
        }
      }
    },
    {
      _id: mongoose.Types.ObjectId("41224d776a326fb40f000005"),
      email: "cheryl@ufl.edu",
      password: "secret",

      fname: "Cheryl",
      lname: "Resch",

      isAdmin: false,
      isStore: false,

      cart: [],
    },
    {
      _id: mongoose.Types.ObjectId("41224d776a326fb40f000006"),
      email: "parker@ufl.edu",
      password: "secret",

      fname: "Parker",
      lname: "Lunn",

      isAdmin: false,
      isStore: false,

      cart: [],
    },
    {
      _id: mongoose.Types.ObjectId("41224d776a326fb40f000007"),
      email: "angel@ufl.edu",
      password: "secret",

      fname: "Angel",
      lname: "Padilla",

      isAdmin: false,
      isStore: true,

      cart: [],

      store: {
        name: "Cognitive Trend",
        description: "We sell marketing services.",
        email: "angel@cognitivetrend.com",
        phone: "+13523523523",
        address: {
          street: "123 SW 2nd Ave",
          city: "Gainesville",
          state: "FL",
          zip: 32607
        }
      }
    },
    {
      _id: mongoose.Types.ObjectId("41224d776a326fb40f000008"),
      email: "stephen@ufl.edu",
      password: "secret",

      fname: "Stephen",
      lname: "Butler",

      isAdmin: false,
      isStore: false,

      cart: [],
    },
    {
      _id: mongoose.Types.ObjectId("41224d776a326fb40f000009"),
      email: "collette@ufl.edu",
      password: "secret",

      fname: "Collette",
      lname: "Oram",

      isAdmin: false,
      isStore: true,

      cart: [],

      store: {
        name: "SPXFlow",
        description: "We make HVAC stuff for large companies.",
        email: "colletteoram@spxflow.com",
        phone: "+11231231234",
        address: {
          street: "1234 SW 6th St",
          city: "Ocala",
          state: "FL",
          zip: 34476
        }
      }
    },
    {
      _id: mongoose.Types.ObjectId("41224d776a326fb40f000010"),
      email: "miranda@ufl.edu",
      password: "secret",

      fname: "Miranda",
      lname: "Ferguson",

      isAdmin: false,
      isStore: false,

      cart: [],
    }
  ]

  /**
   * Listing seeds
   */

const listings = [
    {
      _id: mongoose.Types.ObjectId("11224d776a326fb40f000000"),
      name: "Super Expensive Drone",
      price: 900000,
      description: "This super expensive drone is super expensive.",
      quantityInStock: 9,
      merchant: mongoose.Types.ObjectId("41224d776a326fb40f000002")
    },
    {
      _id: mongoose.Types.ObjectId("11224d776a326fb40f000001"),
      name: "Limited Edition Taylor Swift Assassin Drone",
      price: 900000,
      description: "It's an assassin drone that's themed like Taylor Swift, NOT a drone made to assassinate Taylor Swift. Those are out of stock.",
      quantityInStock: 9,
      merchant: mongoose.Types.ObjectId("41224d776a326fb40f000002")
    },
    {
      _id: mongoose.Types.ObjectId("11224d776a326fb40f000002"),
      name: "Chocolate Chip Cookie",
      price: 3,
      description: "This tastes better than any chocolate chip cookie you've ever had.",
      quantityInStock: 47,
      merchant: mongoose.Types.ObjectId("41224d776a326fb40f000003")
    },
    {
      _id: mongoose.Types.ObjectId("11224d776a326fb40f000003"),
      name: "Red Velvet Cookie",
      price: 3,
      description: "Made with real velvet.",
      quantityInStock: 76,
      merchant: mongoose.Types.ObjectId("41224d776a326fb40f000003")
    },
    {
      _id: mongoose.Types.ObjectId("11224d776a326fb40f000004"),
      name: "Vanilla Creme Cookie",
      price: 3,
      description: "Once again, purchased from a CSE vending machine!",
      quantityInStock: 2321,
      merchant: mongoose.Types.ObjectId("41224d776a326fb40f000003")
    },
    {
      _id: mongoose.Types.ObjectId("11224d776a326fb40f000005"),
      name: "Peanut Butter Cookie",
      price: 3,
      description: "Peanut butter is not real butter.",
      quantityInStock: 24,
      merchant: mongoose.Types.ObjectId("41224d776a326fb40f000003")
    },
    {
      _id: mongoose.Types.ObjectId("11224d776a326fb40f000006"),
      name: "Above & Beyond",
      price: 1,
      description: "Bae",
      quantityInStock: 1,
      merchant: mongoose.Types.ObjectId("41224d776a326fb40f000004")
    },
    {
      _id: mongoose.Types.ObjectId("11224d776a326fb40f000007"),
      name: "Armin",
      price: 1,
      description: "Eurotrance",
      quantityInStock: 1,
      merchant: mongoose.Types.ObjectId("41224d776a326fb40f000004")
    },
    {
      _id: mongoose.Types.ObjectId("11224d776a326fb40f000008"),
      name: "Marketing - Platinum",
      price: 20000,
      description: "Our best package!",
      quantityInStock: 1,
      merchant: mongoose.Types.ObjectId("41224d776a326fb40f000005")
    },
    {
      _id: mongoose.Types.ObjectId("11224d776a326fb40f000009"),
      name: "Marketing - Gold",
      price: 10000,
      description: "Our second best package!",
      quantityInStock: 1,
      merchant: mongoose.Types.ObjectId("41224d776a326fb40f000005")
    },
    {
      _id: mongoose.Types.ObjectId("11224d776a326fb40f000010"),
      name: "Small air purifier",
      price: 1000,
      description: "Breathe better.",
      quantityInStock: 23,
      merchant: mongoose.Types.ObjectId("41224d776a326fb40f000006")
    },
    {
      _id: mongoose.Types.ObjectId("11224d776a326fb40f000011"),
      name: "Large air dryer",
      price: 15000,
      description: "Used to dry your air.",
      quantityInStock: 2,
      merchant: mongoose.Types.ObjectId("41224d776a326fb40f000006")
    },
  ]

const orders = [
  {
    shippingAddress: {
      street: "1234 SW 6th St",
      city: "Ocala",
      state: "FL",
      zip: 34476
    },
    cart: [{"_id":"5cba23c3bdde8715056048e9","quantity":1,"listing":"11224d776a326fb40f000001"},{"_id":"5cba282e294af5198d447ccf","quantity":1,"listing":"11224d776a326fb40f000000"}],
    status: "Delivered",
    user: mongoose.Types.ObjectId("41224d776a326fb40f000001")
  },
  {
    shippingAddress: {
      street: "234 NW 2th Std",
      city: "Marston",
      state: "FL",
      zip: 34276
    },
    cart: [{"_id":"5cba23c3bdde8715056048e9","quantity":2,"listing":"11224d776a326fb40f000001"}],
    status: "Delivered",
    user: mongoose.Types.ObjectId("41224d776a326fb40f000002")
  },
  {
    shippingAddress: {
      street: "321 SE 3rd St",
      city: "Gainesville",
      state: "FL",
      zip: 32601
    },
    cart: [{"_id":"5cba23c3bdde8715056048e9","quantity":3,"listing":"11224d776a326fb40f000004"},{"_id":"5cba282e294af5198d447ccf","quantity":12,"listing":"11224d776a326fb40f000005"}],
    status: "In Transit",
    user: mongoose.Types.ObjectId("41224d776a326fb40f000004")
  }
]

const main = () => {
  seed('users', users);
  seed('listing', listings);
  seed('order', orders);

}

main();
